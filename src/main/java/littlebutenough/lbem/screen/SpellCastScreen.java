package littlebutenough.lbem.screen;

import littlebutenough.lbem.LittleButEnoughMagic;
import littlebutenough.lbem.item.MagicBookItem;
import littlebutenough.lbem.item.ModItems;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;

public class SpellCastScreen implements HudRenderCallback {
    private static final Identifier SPELL_CASTING_BACKGROUND = new Identifier(LittleButEnoughMagic.MOD_ID,
            "textures/gui/spell_casting_bg.png");
    private static final Identifier SPELL_CASTING_FOREGROUND = new Identifier(LittleButEnoughMagic.MOD_ID,
            "textures/gui/spell_casting_fg.png");
    private static final Identifier FIRE_BOLT_SPELL = new Identifier(LittleButEnoughMagic.MOD_ID,
            "textures/gui/fire_bolt.png");

    private static final Identifier BLOOM_SPELL = new Identifier(LittleButEnoughMagic.MOD_ID,
            "textures/gui/bloom.png");
    private static final Identifier FAIL_SPELL = new Identifier(LittleButEnoughMagic.MOD_ID,
            "textures/gui/fail.png");
    @Override
    public void onHudRender(DrawContext drawContext, float tickDelta) {
        MinecraftClient client = MinecraftClient.getInstance();
        PlayerEntity player = client.player;
        int width = client.getWindow().getScaledWidth();
        int height = client.getWindow().getScaledHeight();
        if (player != null && player.isUsingItem() && player.getMainHandStack().getItem() == ModItems.MAGIC_BOOK){
            MagicBookItem magicBookItem = (MagicBookItem) player.getMainHandStack().getItem();
            drawContext.drawTexture(SPELL_CASTING_BACKGROUND, (width / 2)-32, (height/2)-32, 0, 0, 64, 64, 64, 64);
            switch (magicBookItem.currentRoll){
                case 1:{
                    drawContext.drawTexture(SPELL_CASTING_FOREGROUND, (width / 2)-32, (height/2)-32, 0, 0, 64, 16, 64, 64);
                    break;
                }
                case 2:{
                    drawContext.drawTexture(SPELL_CASTING_FOREGROUND, (width / 2), (height/2)-16, 32, 16, 32, 32, 64, 64);
                    break;
                }
                case 3:{
                    drawContext.drawTexture(SPELL_CASTING_FOREGROUND, (width / 2)-32, (height/2)+16, 0, 48, 64, 16, 64, 64);
                    break;
                }
                case 4:{
                    drawContext.drawTexture(SPELL_CASTING_FOREGROUND, (width / 2)-32, (height/2)-16, 0, 16, 32, 32, 64, 64);
                    break;
                }
                default: break;
            }
            if(magicBookItem.cast_tick>0){
                switch (magicBookItem.spellUsed){
                    case "fire_bolt":{
                        drawContext.drawTexture(FIRE_BOLT_SPELL, (width / 2)-32, (height/2)-32, 0, 0, 64, 64, 64, 64);
                        break;
                    }
                    case "bloom":{
                        drawContext.drawTexture(BLOOM_SPELL, (width / 2)-32, (height/2)-32, 0, 0, 64, 64, 64, 64);
                        break;
                    }
                    default:{
                        drawContext.drawTexture(FAIL_SPELL, (width / 2)-32, (height/2)-32, 0, 0, 64, 64, 64, 64);
                        break;
                    }
                }
            }
        }
    }
}
