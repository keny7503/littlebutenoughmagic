package littlebutenough.lbem.datagen;

import littlebutenough.lbem.item.ModItems;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.minecraft.data.server.recipe.RecipeJsonProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder;
import net.minecraft.data.server.recipe.ShapelessRecipeJsonBuilder;
import net.minecraft.item.Items;
import net.minecraft.recipe.book.RecipeCategory;
import net.minecraft.util.Identifier;

import java.util.function.Consumer;

public class ModRecipeProvider extends FabricRecipeProvider {
    public ModRecipeProvider(FabricDataOutput output) {
        super(output);
    }

    @Override
    public void generate(Consumer<RecipeJsonProvider> exporter) {
//        ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, ModItems.MAGIC_BOOK)
//                .pattern(" l ")
//                .pattern("lbl")
//                .pattern(" l ")
//                .input('b', Items.ENCHANTED_BOOK)
//                .input('i', Items.LAPIS_LAZULI)
//                .criterion(FabricRecipeProvider.hasItem(Items.ENCHANTED_BOOK),
//                        FabricRecipeProvider.conditionsFromItem(Items.ENCHANTED_BOOK))
//                .offerTo(exporter,new Identifier(getRecipeName(ModItems.MAGIC_BOOK)));
        ShapedRecipeJsonBuilder.create(RecipeCategory.COMBAT, ModItems.MAGIC_BOOK).pattern("lll").pattern("lbl").pattern("lll")
                .input('b', Items.ENCHANTED_BOOK)
                .input('l', Items.LAPIS_LAZULI)
                .criterion(FabricRecipeProvider.hasItem(Items.ENCHANTED_BOOK),
                        FabricRecipeProvider.conditionsFromItem(Items.ENCHANTED_BOOK))
                .offerTo(exporter);
    }
}
