package littlebutenough.lbem.item;

import littlebutenough.lbem.LittleButEnoughMagic;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroupEntries;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModItems {
    public static final MagicBookItem MAGIC_BOOK = new MagicBookItem(new FabricItemSettings().maxCount(1).maxDamage(250));
    public static Item registerItem(String name, Item item){
        return Registry.register(Registries.ITEM, new Identifier(LittleButEnoughMagic.MOD_ID,name),item);
    }
    private static void addItemsToCombatItemGroup(FabricItemGroupEntries entries) {
        entries.add(MAGIC_BOOK);

    }
    public static void registerModItems() {
        LittleButEnoughMagic.LOGGER.info("Registering Mod Items for " + LittleButEnoughMagic.MOD_ID);
        Registry.register(Registries.ITEM, new Identifier(LittleButEnoughMagic.MOD_ID,"magic_book"),MAGIC_BOOK);
        ItemGroupEvents.modifyEntriesEvent(ItemGroups.COMBAT).register(ModItems::addItemsToCombatItemGroup);
    }
}
