package littlebutenough.lbem.item;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.screen.v1.ScreenMouseEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.StackReference;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.Slot;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.LiteralTextContent;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.world.World;
import org.lwjgl.glfw.GLFW;

import java.util.*;

public class MagicBookItem extends Item {
    public enum Element{PYRE,AQUA,TERRA,AERO}
    public List<Element> fire_bolt_combo = new ArrayList<>(Arrays.asList(Element.PYRE,Element.PYRE));
    public Map<String,List<Element>> comboMap = new HashMap<String,List<Element>>();
    public int fire_bolt_combo_cast_time = 10;
    public String spellUsed = "";

    public int cast_tick = 0;
    public int currentRoll = 4;
    private boolean attackLift = true;
    public List<Element> combo = new ArrayList<Element>();

    public MagicBookItem(Settings settings) {
        super(settings);
        comboMap.put("fire_bolt",new ArrayList<>(Arrays.asList(Element.PYRE,Element.PYRE)));
        comboMap.put("bloom",new ArrayList<>(Arrays.asList(Element.TERRA,Element.AQUA)));

    }

    @Override
    public UseAction getUseAction(ItemStack stack) {
        return UseAction.NONE;
    }
    @Override
    public int getMaxUseTime(ItemStack stack) {
        return 72000;
    }

    @Override
    public boolean isDamageable() {
        return true;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        user.setCurrentHand(hand);
        this.getDefaultStack().damage(1, user, e -> e.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND));
        return TypedActionResult.pass(itemStack);
    }

    @Override
    public void usageTick(World world, LivingEntity user, ItemStack stack, int remainingUseTicks) {
        super.usageTick(world, user, stack, remainingUseTicks);
        if(world.isClient() && remainingUseTicks%5 == 0){
            if(currentRoll<4){
                currentRoll++;
            }else {
                currentRoll =1;
            }
        }
        if(MinecraftClient.getInstance().options.jumpKey.isPressed()){
            if(!attackLift) return;
            attackLift = false;
            switch (currentRoll){
                case 1:{
                    combo.add(Element.PYRE);
                    user.playSound(SoundEvents.ENTITY_BLAZE_SHOOT,1.0F,1.0F);
                    break;}
                case 2:{ combo.add(Element.AQUA);
                    user.playSound(SoundEvents.ENTITY_PLAYER_SPLASH,1.0F,1.0F);
                    break;}
                case 3:{ combo.add(Element.TERRA);
                    user.playSound(SoundEvents.BLOCK_POINTED_DRIPSTONE_LAND,1.0F,1.0F);
                    break;}
                case 4:{ combo.add(Element.AERO);
                    user.playSound(SoundEvents.ENTITY_ENDER_DRAGON_FLAP,1.0F,1.0F);
                    break;}
            }
        }else {attackLift = true;}
        if(!world.isClient()){
            if(cast_tick>0){cast_tick--;}
            System.out.println(combo.toString());
            for (Map.Entry<String, List<Element>> entry : comboMap.entrySet()){
                if (combo.equals(entry.getValue())){
                    System.out.println(entry.getKey());
                    spellUsed = entry.getKey();
                    cast_tick = fire_bolt_combo_cast_time;
                    combo.clear();
                    return;
                }
            }
            for (Map.Entry<String, List<Element>> entry : comboMap.entrySet()){
                if (isPrefix(combo,entry.getValue())) {
                    return;
                }
            }
            combo.clear();
            spellUsed = "fail";
            cast_tick = fire_bolt_combo_cast_time;
            System.out.println("FAIL");
        }

    }


    @Override
    public void onStoppedUsing(ItemStack stack, World world, LivingEntity user, int remainingUseTicks) {
        super.onStoppedUsing(stack, world, user, remainingUseTicks);
        currentRoll = 4;
        combo.clear();
    }
    public static <T> boolean isPrefix(List<T> prefix, List<T> list) {
        if (prefix.size() > list.size()) {
            return false;
        }

        return list.subList(0, prefix.size()).equals(prefix);
    }

}
